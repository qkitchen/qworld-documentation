# Code of Ethics and Code of Conduct

## Basics

Version: *01.01.2020*

These documents are not an exhaustive list of things that we should follow or that we cannot do. Rather, take them in the spirit in which it's intended - some guides to make it easier to enrich all of us and each event or project or communication or channel in which we participate.

### Modifications

Any modification on Code of Ethics and Code of Conduct is prepared by the Ethics committee of QWorld and then presented to the board of QWorld. Any change is approved and published if at least 80% of the votes are positive among the board members. Otherwise, if at least half of the board are positive, then the board organizes a voting among the leading members, and then any change is approved and published if at least 66% of the votes are positive among the leading members.

For any suggestions, please contact the Ethics committee of QWorld, who should process the suggestion and then acknowledge the sender about their decisions.

### Definitions

A person can become **a member of QWorld** as long as they share the values of QWorld and take an active role such as taking a role/title/responsibility in QWorld or in a QWorld channel or in a project or an event partially or fully belonging to some QWorld channels or groups.

*   An indicator of a member is the appearance of the name of the person on the website of QWorld or QWorld channels or QWorld groups.
*   Some examples of roles/titles/responsibilities: coordinator, leader, project leader, project coordinator, event/workshop leader, educator/qeducator, mentor/qmentor, designer/qdesigner, (local or main) event organizer, project-specific titles like QDrivers.

A **collaborator**is a person
*   who is not a member of QWorld,

but

*   who takes part in an event or a project organized or co-organized or supported under the name of QWorld or by some QWorld channels.

A **participant** is a person

*   who applies to an event or a project (by filling an application form or by using any kind of communication channels or by taking a similar action) organized or co-organized or supported under the name of QWorld or by some QWorld channels or QWorld groups;
*   who is a participant of an event or a project organized or co-organized or supported under the name of QWorld or by some QWorld channels or QWorld groups;

or,

*   who contacts/approaches to some QWorld channels, groups, or members by using any kind of communication.

A **minor** is any person younger than 18 years old.

**Harassment is any unwanted behavior including**

*   any offensive comments about gender, gender identity, and expression, sexual orientation, disability, mental illness, neuro(a)typicality, physical appearance, body size, race, or religion;
*   any unwelcome comments about a person's lifestyle choices and practices (personal and professional choices);
*   gratuitous or off-topic sexual images or behavior in any QWorld spaces of communication and activity;
*   any physical contact and simulated physical contact (e.g., textual descriptions in communications like “*hug*” or “*backrub*”) without consent or after a request to stop;
*   threats of violence or incitement of violence towards any participant in educational activities or a member of the community of QWorld;
*   any actions aimed at deliberate intimidation, stalking or following QWorld participants and members;
*   taking photos or recordings during events related to QWorld activities to harass or intimidate the participants;
*   logging online activity, saving conversations in Slack and other spaces of QWorld to harass or intimidate the participants;
*   unwelcome sexual attention and any requesting or assuming inappropriate levels of intimacy with others;
*   continued one-on-one communication after requests to cease;
*   any behavior that interferes with QWorld's activity, e.g., sustained disruption of discussion;
*   publishing or sharing with third parties non-harassing private communication without the permission of the person concerned;
*   deliberate “outing” of any aspect of a person's identity without their consent except as necessary to protect other QWorld members or other vulnerable people from intentional abuse.

### Violation of the Code of Ethics or Code of Conduct

Violation of the code of ethics or code of conduct by any member may result in termination or dismissal from QWorld and its channels and groups.

Violation of the code of ethics or code of conduct by any collaborator (person or entity) may result in termination of the collaboration with QWorld and its channels and groups.

Violation of the code of ethics or code of conduct by any participant may result in being asked to leave the event/project.

For each event/project of QWorld, specific actions are defined and applied in the corresponding code of conduct.

### Reporting

If experiencing

*   **any violation of code of ethics or code of conduct**,
*   particularly **any abuse or harassment**,

**any other concern,** or **any other ethics-related issues, **

Please ***contact immediately***

*   QWorld members who are responsible for or in charge of the corresponding event/project.The contact persons should be clearly indicated on the website and application form of the event/project, or

*   some member(s) of QWorld's ethics committee (we will respect confidentiality requests for the purpose of protecting victims of abuse).
