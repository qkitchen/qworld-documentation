# Code of Ethics and Code of Conduct

## Code of Conduct

Version: *01.01.2020*

Our “Code of Conduct” is publicly available including the previous versions.

Additional rules and regulations may be defined by each QWorld channel, QWorld group or for a specific event or project.

*   In case of a conflict with the code of conduct, with the approval of the Ethics Committee of QWorld, the proposed rule or regulation may be accepted by providing a public explanation. 
*   In any other case, some or all additional rules or regulations may not be publically available.

The QWorld community is dedicated to providing a **harassment-free experience** to all participants of activities, events, and projects organized by QWorld, members of the QWorld community, and all the people associated with the conducted activities regardless of gender, gender identity, and expression, sexual orientation, disability, physical appearance, body size, race, or religion.

This code of conduct applies to all activity spaces managed by QWorld, including our website, blog, Slack workspace, twitter page, Facebook page, events, projects, training, and conferences organized (co-organized or supported) by QWorld, and any communication in the QWorld community (online and offline).

Anyone who violates this Code of Conduct may be sanctioned or expelled from the community of QWorld at the discretion of The Ethics Committee of QWorld. **Members of QWorld are responsible for knowing and abiding by code of conduct rules.**

### Communication among QWorld members, collaborators, and participants

Every member of QWorld should work to create a welcoming, friendly, harassment-free, and respectful environment during the events/projects. All members of QWorld should:

- maintain a safe, responsible, and calm behavior;

- be on time at the events and meetings, both online and offline;

- prepare all materials needed for events;

- respect our hosts and event participants;

- dress according to QWorld's rules.

We recommend QWorld members to include all relevant people to any communication to make the communication more formal.

The default communication channel among QWorld members (besides face-to-face meetings) is email and QWorld's Slack workspace. Remember that any message in our Slack workspace cannot be deleted and no modification can be made after 1 minute.

The default communication channel among QWorld members and collaborators (besides face-to-face meetings) is email and video conference.

The default communication channel between the participants and the members of QWorld and its collaborators (besides meeting during the event/project) is email. 

*   For any generic announcement, each participant's email must be hidden by putting it in BCC and the emails of all relevant organizers should be put in CC.
*   For a communication specifically addressed to a group of participants, at least one relevant person from QWorld and at least one relevant person from the collaborator side (if any) must be added in CC.

If the communication by video conference is allowed and defined under a project/event, then each participant has the right to record the meeting but he or she must inform the other party and also share the recorded video with the other party after the conversation.

Any video recording of an online meeting by a member of QWorld or a collaborator related to a QWorld event/project should be defined under the event/project webpage and the other party should be informed and the record should be shared with the other party after the recording.

Contact info (e.g., phone number, home address, social media accounts) of any participant may not be asked by any member of QWorld for a purpose not related to QWorld's aims or activities. On the other hand, any member of QWorld or a collaborator may share his or her contact info with a participant who is not a minor, upon request by the participant.

Any connection request through social media or similar channel from any minor participant may not be accepted by any member of QWorld or a collaborator,  excluding the cases when the minors are known in the personal network.

### Minors (under 18 years of age)

QWorld members and collaborators must make every effort to protect the rights of minors participating QWorld events and projects. 

All private relationships, private communications (including social media channels), or sexual contacts with minor participants are prohibited for QWorld's members and collaborators.

All rules of this Code of Conduct apply to communication with minors, and in addition, participation in the events/projects of minors should take place according to the following principles:

*   Communication with minors should take place only through general communication channels (excluding all private channels), and the only exceptions are defined below
*   for any minor participating in our activities, participation consent signed by a legal guardian is required
    *   the form for consent should contain basic data about the event, time and place of the event, details of the institution conducting the event and contact details of the person/people responsible for organizing the event
    *   the form should be both in English and official language of the place where the event/project is held
    *   the form for consent to participate in workshops should be available for download on the website of the event/project and on the application form
    *   the form for consent should include consent to use of photos and materials obtained during the workshop, e.g., photos from workshops used to promote the event on social media channels and the website of QWorld
    *   the form for consent should include consent to use the name, surname and email of the minor by QWorld for contacting the minor by using email regarding QWorld related events, projects, news, and similar activities 
    *   some example forms can be accessed from [this link](http://qworld.lu.lv/index.php/the-example-forms-of-consent-for-minors/)

In case of working with a minor or a group of minors individually for a scientific or pedagogical purpose:

*   the default communication channel is email;
*   for each minor, a legal guardian or an administrator from the minor's school must be included in all email communications;
*   any online or offline meeting must be arranged through a communication described in the above item and each legal guardian must be informed about the meeting(s);
*   any meeting should be held in an educational institute, preferable in public space or in an office by keeping the door open.

### Organizing an event or project

Any collaborator of the event or project is asked to accept the code of ethics and the code of conduct. Otherwise, the collaboration should not be established.

The collaborators (if any) should be explicitly and publicly written.

The following should be explicitly written on the project or event web page and on any related document:

*   a link to this document
*   a specific code of conduct for the event/project (some examples are given at the end of this document)
*   the communication channel(s)
*   each member of QWorld taking a role/title/responsibility in the event or project
*   the venue(s) and exact times/dates

The event/project-specific code of conduct should be placed at the top of the application form in a concise form.

The permission for taking photos or recording movies during the event and then using them in QWorld's social media, news, newsletters, or similar places should be asked in the application form. The same permission can also be asked from QWorld's organizer group or channel.

The permission for storing and processing personal data (name, surname, email, gender, school/university, faculty/department, grade/level, company, and opinions about QWorld's activities) by QWorld and QWorld's partners (with permission of QWorld)  should be asked in the application form.

If minor applicants are not eligible to participate in the event, then it should be explicitly stated and each applicant should confirm that he or she is over 18 in the application form.

If minor applicants are eligible to participate in the event, each applicant should state whether he or she is younger than 18 years old or not and if the applicant is younger than 18 years old, then it should be explicitly stated that a consent for participation signed by a legal guardian is required. The participant should confirm that he or she will send the photo of the signed consent to the specified email address and then bring the original copy to the event if he or she is accepted to the event.

### Personal information shared by a participant

Personal information shared by any participant cannot be used for any purpose not related to QWorld's aim or activities.

More information will be given in the privacy policy of QWorld.

### Code of Conduct in social media channels

Social media channels are one of the most popular ways of communicating with participants today. We created our social media channels (Facebook, Twitter, Slack) to disseminating knowledge about the activities of QWorld, supporting our marketing activities, and foster online communities of QWorld, where people can share ideas and get inspired. All rules of this Code of Conduct apply to communication via social media channels, and in addition, all members of QWorld and our collaborators should remember:

*   abuse of intellectual property rights, including copyright, is forbidden
*   publication of data containing sensitive personal information of QWorld members, participants, or anyone else is forbidden
*   the Ethics Committee of QWorld or moderators or coordinators can remove any content or comments that are offensive, have signs of harassment or are spam
*   Law-breaking attitudes and any behavior encouraging others to break the laws, regulations or rules (including those of any social media channel) are forbidden
*   show respect for all members of QWorld online community
*   photos and recordings taken during a QWorld event can be published only with the permission of the participants. When using photos from the internet, use photos with one of the two licenses:

    1. CC BY - the photos can be used by indicating the author
    2. CC0 - the photos can be freely used without having to acknowledge the authorship

*   without the knowledge about the status of the photos (licenses) - do not use it

QWorld reserves the rights to ban from our social media channels any person who violates our Code of Ethics or Code of Conduct. When you encounter a question/comment that is not appropriate to share on the public social media channel, please send us a direct message or email: public-relation [at] qworld.lu.lv

