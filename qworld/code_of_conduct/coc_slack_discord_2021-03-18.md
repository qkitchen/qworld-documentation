# Code of Conduct for QWorld's Slack and Discord workspace

Version: *January 01, 2020*

QWorld's Slack and Discord workspaces are channels that allows simple and friendly communications of QWorld's members and anyone interested in quantum related scientific topics or computation or software or technologies or QWorld's activities.

The QWorld's Slack and Discord channels aim at creating a community for discussion about quantum software and quantum technologies. Any conversation, public or private, not related to the topic may be removed. In particular, it includes any form of advertisement, political or religious discussion, etc.

## Purpose

A primary goal of QWorld is to be inclusive to the largest number of participants, with the most varied and diverse backgrounds possible. As such, we are committed to providing a friendly, safe and welcoming environment for all, regardless of gender, sexual orientation, ability, ethnicity, socioeconomic status, and religion (or lack thereof).

This isn't an exhaustive list of things that you can't do. Rather, take it in the spirit in which it's intended - a guide to make it easier to enrich all of us and the technical communities in which we participate.

This Code of Conduct outlines our expectations for all those who participate in our Slack or Discord communities, as well as the consequences for unacceptable behavior.

We invite all those who participate in QWorld activities to help us create a safe and positive experience for everyone.

## Creating a Welcoming and Inclusive Culture

Communities mirror the societies in which they exist and positive action is essential to counteract the many forms of inequality and abuses of power that exist in society.

If you see someone who is making an extra effort to ensure our community is welcoming, friendly, and encourages all participants to contribute to the fullest extent, please recognize their efforts.

### Expected Behavior

The following behaviors are expected and requested of all community members:

*   Be friendly and patient.
*   Be welcoming. We strive to be a community that welcomes and supports people of all backgrounds and identities. This includes, but is not limited to members of any race, ethnicity, culture, national origin, color, immigration status, social and economic class, educational level, sex, sexual orientation, gender identity and expression, age, size, family status, political belief, religion, and mental and physical ability.
*   Be considerate. Your work can be used by other people, and you, in turn, will depend on the work of others. Any decision you take will affect users and colleagues, and you should take those consequences into account when making decisions. Remember that we're a world-wide community, so you might not be communicating in someone else's primary language.
*   Be respectful. Not all of us will agree all the time, but disagreement is no excuse for poor behavior and poor manners. We might all experience some frustration now and then, but we cannot allow that frustration to turn into a personal attack. It's important to remember that a community where people feel uncomfortable or threatened is not a productive one. Members of the Creative Commons community should be respectful when dealing with other members as well as with people outside the QWorld community.
*   Be careful in the words that you choose. We are a community of professionals, and we conduct ourselves professionally. Be kind to others. Do not insult or put down other participants. Harassment and other exclusionary behavior aren't acceptable. This includes, but is not limited to:
    *   violent threats or language directed against another person;
    *   discriminatory jokes and language;
    *   posting sexually explicit or violent material;
    *   posting (or threatening to post) other people's personally identifying information ("doxing");
    *   personal insults, especially those using racist or sexist terms;
    *   unwelcome sexual attention;
    *   advocating for, or encouraging, any of the above behavior;
    *   repeated harassment of others. In general, if someone asks you to stop, then stop.
*   When we disagree, try to understand why. Disagreements, both social and technical, happen all the time and QWorld is no exception. It is important that we resolve disagreements and differing views constructively. Remember that we're different. The strength of QWorld comes from its varied community, people from a wide range of backgrounds. Different people have different perspectives on issues. Being unable to understand why someone holds a viewpoint doesn't mean that they're wrong. Don't forget that it is human to err and blaming each other doesn't get us anywhere. Instead, focus on helping to resolve issues and learning from mistakes.
*   Moderators and Coordinators reserve the right to delete excessive self-promotional or commercial posts.
*   Content that has been identified as objectionable, inappropriate, or off-topic will be subject to deletion by channel moderators. Posters will receive a warning and risk being blocked from the channel if unacceptable behavior persists.

These decisions are made by QWorld in its sole discretion.

## Consequences of Unacceptable Behavior

Unacceptable behavior from any community member, including staff and those with decision-making authority, will not be tolerated.

Anyone asked to stop unacceptable behavior is expected to comply immediately.

If a community member engages in unacceptable behavior, we may take any action deemed appropriate, up to and including a temporary ban or permanent expulsion from the community without warning.

## Reporting Guidelines

If you are subject to or witness unacceptable behavior, or have any other concerns, please notify us as soon as possible by emailing the coordinator or pinging a QWorld staff member on Slack or Discord. You may also directly contact the Ethics Committee of QWord.

All complaints will be investigated and appropriate steps will be taken to keep the Slack and Discord communities safe and friendly, which may include contacting the person responsible for improper content if necessary. All coordinators and other involved folks will maintain confidentiality with regard to the reporter of an incident, however, for our purposes we require proof of unwelcome behavior especially for private communication.

To discuss a decision, contact the coordinator or the Ethics Committee of QWord and we will review your question.

## Incident Response

If the coordinator agrees that the message violates the Code of Conduct, the message is deleted with an appropriate comment. In case of regular or serious breaking of Code of Conduct, the coordinator is allowed to deactivate the account. Please be aware that violating the QWorld Slack and Discord Code of Conduct implies violating the QWorld Code of Conduct, hence additional steps may be taken resulting from the latter Code of Conduct.

## Modification

The QWorld is allowed to modify this Code of Conduct. The coordinators of Slack and Discord are responsible for informing these communities by posting an appropriate comment on the #general channel and providing the link to modified file.

## License and attribution

This code of conduct was directly adapted from Slack/Code of Conduct of Creative Commons [https://wiki.creativecommons.org/wiki/Slack/Code_of_Conduct](https://wiki.creativecommons.org/wiki/Slack/Code_of_Conduct), which was directly adopted from the Stumptown Syndicate (http://stumptownsyndicate.org/code-of-conduct/) and distributed under a Creative Commons Attribution-ShareAlike license. It was also noted that "Major portions of text derived from the Django Code of Conduct and the Geek Feminism Anti-Harassment Policy".