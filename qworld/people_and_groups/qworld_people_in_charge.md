# QWorld's Poeple in Charge

In this document, we list our current and former members in charge with different duties and responsibilities under QWorld. Besides the names, we present their duties/responsibilities with the durations and also the corresponding email addresses.

**[QWorld](#qworld):**
* [The Ethics Committee of QWorld](#the-ethics-committee-of-qworld)
* [QBoard: The Board of QWorld](#qboard-the-board-of-qworld)
* [QWorld's Public Relation Team](#qworlds-public-relation-team)
* [QWorld's Designer](#qworlds-designer)
* [QWorld's Slack Workspace](#qworlds-slack-workspace)
* [QWorld's QKitchen Gitlab Repository](#qworlds-qkitchen-gitlab-repository)

**[QWorld's Channels](#qworlds-channels):**
* [QCousins](#qcousins)
* [QWomen](#qwomen)
* [QKitchen](#qkitchen)
* [QJunior](#qjunior)
* [QUniversity](#quniversity)
* [QMentor Training](#qmentor-training)


# QWorld


## The Ethics Committee of QWorld

* Janos Asboth (janos.asboth [at] gmail.com), November 2019 - present
* Jarosław Miszczak (jarek [at] miszczak.eu), November 2019 - present
* Özlem Salehi Köken (ozlemsalehi [at] gmail.com), November 2019 - present
* Agnieszka Wolska (agnieszka.k.wolska [at] gmail.com), November 2019 - present


## QBoard: The Board of QWorld

The Board of QWorld is formed by all coordinators including the ones from QWorld’s channels (see below).

* Abuzer Yakaryilmaz (info [at] qworld.lu.lv), November 2019 - present
* Maksims Dimitrijevs, November 2019 - present
* Adam Glos, November 2019 - present
* Paweł Gora, November 2019 - present
* Özlem Salehi Köken, November 2019 - present
* Agnieszka Wolska, November 2019 - present
* Zoltán Zimborás, November 2019 - present


## QWorld's Public Relation Team

**The PR coordinator:** Agnieszka Wolska (pr-head [at] qworld.lu.lv), April 2020 - present

**The PR team members** (pr-team [at] qworld.lu.lv):
 
* Adam Glos, November 2019 - present
* Anastasija Trizna, May 2020 - present
* Agnieszka Wolska, November 2019 - present


\
_The former PR coordinator: Paweł Gora, November 2019 - March 2020_

_The former PR team members:_ 
* _Zeki Seskir, November 2019 - March 2020_
* _Zoltán Zimborás, November 2019 - March 2020_


## QWorld's Designer

**The designer:** Agnieszka Wolska (qdesigner [at] qworld.lu.lv), February 2019 - present


## QWorld's Slack Workspace

https://qworldworkspace.slack.com/

**The admin:** Adam Glos (admin-slack [at] qworld.lu.lv), January 2020 - present

\
_The former admin: Paweł Gora, September 2019 - December 2020_


## QWorld's QKitchen Gitlab Repository

https://gitlab.com/qkitchen

**The maintainer:** Adam Glos, August 2019 - present

\
_The former maintainer: Abuzer Yakaryilmaz, July 2019 - March 2020_




# QWorld's Channels

## QCousins

**Contact:** qcousins [at] qworld.lu.lv

**The coordinators:**
* Maksims Dimitrijevs, January 2020 - present
* Paweł Gora, August 2019 - present
* Zoltán Zimborás, January 2020 - present 

\
_The former coordinator: Abuzer Yakaryilmaz, August 2019 - January 2020_


## QWomen

**Contact:** qwomen [at] qworld.lu.lv

**The coordinators:**
* Özlem Salehi Köken, November 2019 - present
* Agnieszka Wolska, January 2020 - present

**The vice coordinator:** Ayşin Taşdelen, April 2020 - present

**The coordinator (only for operational tasks):** Abuzer Yakaryilmaz, February 2020 - present

\
_The former coordinator: Abuzer Yakaryilmaz, November 2019 - January 2020_


## QKitchen

**Contact:** qkitchen [at] qworld.lu.lv

**The coordinators:**

* Adam Glos, August 2019 - present
* Zoltán Zimborás, November 2019 - present

\
_The former coordinator: Abuzer Yakaryilmaz, August 2019 - March 2020_


## QJunior

**Contact:** qjunior [at] qworld.lu.lv

**The coordinator:**  Abuzer Yakaryilmaz, November 2019 - present


## QUniversity

**Contact:** quniversity [at] qworld.lu.lv

**The coordinators:**

* Abuzer Yakaryilmaz, November 2019 - present
* Zoltán Zimborás, November 2019 - present


## QMentor Training

**Contact:** qmentor-training [at] qworld.lu.lv

**The coordinators:**

* Maksims Dimitrijevs, November 2019 - present
* Özlem Salehi Köken, November 2019 - present
