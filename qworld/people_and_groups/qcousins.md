# QCousins

In this document, we list QCousins in the order of joining the network (entanglement).

http://qworld.lu.lv/index.php/qcousins/

## QLatvia

http://qworld.lu.lv/index.php/qlatvia/

qlatvia [at] qworld.lu.lv

Founded on August 2018.

## QTurkey

http://qworld.lu.lv/index.php/qturkey/

qturkey [at] qworld.lu.lv

Founded on May 2019.

## QHungary

http://qworld.lu.lv/index.php/qhungary/

qhungary [at] qworld.lu.lv

Founded on July 2019.

## QBalkan

http://qworld.lu.lv/index.php/qbalkan/

qbalkan [at] qworld.lu.lv

Founded on July 2019.

## QPoland

http://qworld.lu.lv/index.php/qpoland/

qpoland [at] qworld.lu.lv

Founded on July 2019.

## QRussia

http://qworld.lu.lv/index.php/qrussia/

qrussia [at] qworld.lu.lv

Founded on November 2019.