# QWorld's leading members

In this document, we list our active (and former) leading members with their active periods.

The email list for leading members: qworld-leading-members [at] googlegroups.com

---

## The list of active leading members (11 people)

| Member  |  Group |  Starting Date |  Termination Date |
|:--------|:-------|:---------------|:------------------|
|Maksims Dimitrijevs|QLatvia|November 11, 2019|November 10, 2020|
|Özlem Salehi Köken|QTurkey|November 11, 2019|November 10, 2020|
|Zeki Seskir|QTurkey|November 11, 2019|November 10, 2020|
|Agnieszka Wolska|QLatvia|November 11, 2019|November 10, 2020|
|Abuzer Yakaryilmaz|QLatvia|November 11, 2019|November 10, 2020|
|Adam Glos|QPoland|February 06, 2020|February 05, 2021|
|Paweł Gora|QPoland|February 06, 2020|February 05, 2021|
|Anastasija Trizna|QLatvia|February 06, 2020|February 05, 2021|
|Berat Yenilen|QTurkey|February 06, 2020|February 05, 2021|
|Zoltán Zimborás|QHungary|February 06, 2020|February 05, 2021|
|Cenk Tüysüz|QTurkey|March 21, 2020|March 20, 2021|

---

## The list of active integration members

| Member  |  Group |  Starting Date |  Termination Date |
|:--------|:-------|:---------------|:------------------|
|Janos Asboth|QHungary|November 11, 2019|November 10, 2020|
|Mohamed Elsayed Yahia|QBalkan|November 11, 2019|November 10, 2020|
|Jarosław Miszczak|QPoland|November 11, 2019|November 10, 2020|


---

## The list of former integration members
| Member  |  Group |  Starting Date |  Termination Date |
|:--------|:-------|:---------------|:------------------|
|Adam Glos|QPoland|November 11, 2019|February 05, 2020|
|Paweł Gora|QPoland|November 11, 2019|February 05, 2020|
|Zoltán Zimborás|QHungary|November 11, 2019|February 05, 2020|

---



