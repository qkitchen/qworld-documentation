**Topic**: The course template

**Authors**: Adam Glos aglos [at] iitis.pl

**Description**: The aim of the project is to present a template for preparing a course. Note that in the README.md I haven't use any HTML commands, but a lot of markdown.

**Requirements**:
*  basics of quantum computing
*  Bronze course finished
*  base 10 numerical system

**Dependencies**
* jupyter
* python 3

**Copyright 2020 QWorld**

The code of this project is licensed under the Apache License, Version 2.0
(the "License"); you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   [http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

The text and figures of this work is licensed under a Creative Commons Attribution 4.0 International License, available at [https://creativecommons.org/licenses/by/4.0/legalcode](https://creativecommons.org/licenses/by/4.0/legalcode).
